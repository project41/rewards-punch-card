/**
 * Allow a logged-in user to see, edit and update her own profile
 * Allow admins to see everyone
 */

module.exports = function(req, res, ok) {

	// Make sure user is currently authenticated
	if (!req.session.User) {
		var noRightsError = [{name: 'noRights', message: 'You must be logged in.'}]
		req.session.flash = {
			err: noRightsError
		}
    res.redirect('/session/new');
    return;
	}

	ok();

};