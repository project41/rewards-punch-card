/**
 * CustomerController
 *
 * @description :: Server-side logic for managing customers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	'new': function(req, res) {
		res.view();
	},
	
	create: function(req, res, next) {
	
	var customerObj = {
	  name: req.param('name'),
	  nicknames: req.param('notes'),
	  email: req.param('email'),
	  punches: req.param('punches')
	}
	
	// Create a Customer with the params sent from 
	// the sign-up form --> new.ejs
	Customer.create(customerObj, function customerCreated(err, customer) {
	
	  // // If there's an error
	  // if (err) return next(err);
	
	  if (err) {
	    console.log(err);
	    req.session.flash = {
	      err: err
	    }
	
	    // If error redirect back to sign-up page
	    return res.redirect('/customer/new');
	  }
	
	  // Let other subscribed sockets know that the customer was created.
	  Customer.publishCreate(customer);
	
	    // After successfully creating the customer
	    // redirect to the show action
	    // From ep1-6: //res.json(customer); 
		
	    res.redirect('/purchase/initialload/' + customer.id);
	});
	},
	
	// render the profile view (e.g. /views/show.ejs)
	show: function(req, res, next) {
	Customer.findOne(req.param('id')).populate('purchases').exec(function foundCustomer(err, customer) {
	  if (err) return next(err);
	  if (!customer) return next();
	  res.view({
	    customer: customer
	    //purchases: populate('purchases')
	  });
	});
	},
	
	index: function(req, res, next) {
	
	// Get an array of all customers in the Customer collection(e.g. table)
	Customer.find(function foundCustomers(err, customers) {
	  if (err) return next(err);
	  // pass the array down to the /views/index.ejs page
	  res.view({
	    customers: customers
	  });
	});
	},
	
	// render the edit view (e.g. /views/edit.ejs)
	edit: function(req, res, next) {
	
	// Find the customer from the id passed in via params
	Customer.findOne(req.param('id'), function foundCustomer(err, customer) {
	  if (err) return next(err);
	  if (!customer) return next('Customer doesn\'t exist.');
	
	  res.view({
	    customer: customer
	  });
	});
	},
	
	// process the info from edit view
	update: function(req, res, next) {
	
	if (req.session.User.admin) {
		var customerObj = {
			name: req.param('name'),
			nicknames: req.param('notes'),
			email: req.param('email'),
			punches: req.param('punches')
		}
	} else {
		var customerObj = {
			name: req.param('name'),
			nicknames: req.param('notes'),
			email: req.param('email')
		}
	}
	
	Customer.update(req.param('id'), customerObj, function customerUpdated(err) {
	  if (err) {
	    return res.redirect('/customer/edit/' + req.param('id'));
	  }
	
	  res.redirect('/customer/show/' + req.param('id'));
	});
	},
	
	// substract punch per purchase
	updatepunches: function(req, res, next) {
		Customer.findOne(req.param('id'), function foundCustomer(err, customer) {
			if (err) return next(err);
			
			if (!customer) return next('Customer doesn\'t exist.');
			
			var remainingPunches = customer.punches - 1;
			var customerObj = {
				punches: remainingPunches
			}
			
			Customer.update(req.param('id'), customerObj, function customerPunchesUpdated(err) {
				if (err) {
				    return res.redirect('/customer/show/' + req.param('id'));
				  }
				  
				  res.redirect('/customer/show/' + req.param('id'));
			});
		});
	},
	
	reloadpunches: function(req, res, next) {
		Customer.findOne(req.param('id'), function foundCustomer(err, customer) {
			if (err) return next(err);
			
			if (!customer) return next('Customer doesn\'t exist.');
			
			var remainingPunches = customer.punches + 12;
			var customerObj = {
				punches: remainingPunches
			}
			
			Customer.update(req.param('id'), customerObj, function customerPunchesUpdated(err) {
				if (err) {
				    return res.redirect('/customer/show/' + req.param('id'));
				  }
				  
				  res.redirect('/customer/show/' + req.param('id'));
			});
		});
	},
	
	destroy: function(req, res, next) {
	
	Customer.findOne(req.param('id'), function foundCustomer(err, customer) {
	  if (err) return next(err);
	
	  if (!customer) return next('Customer doesn\'t exist.');
	
	  Customer.destroy(req.param('id'), function customerDestroyed(err) {
	    if (err) return next(err);
	
	    // Inform other sockets (e.g. connected sockets that are subscribed) that this customer is now logged in
	    Customer.publishUpdate(customer.id, {
	      name: customer.name,
	      action: ' has been destroyed.'
	    });
	
	    // Let other sockets know that the customer instance was destroyed.
	    Customer.publishDestroy(customer.id);
	
	  });        
	
	  res.redirect('/customer');
	
	});
	}
};

