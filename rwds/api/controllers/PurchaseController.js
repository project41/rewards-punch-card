/**
 * PurchaseController
 *
 * @description :: Server-side logic for managing purchases
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	'new': function(req, res) {
		res.view('purchase/new', {
			customerID: req.param('id')
		});
	},
	
	create: function(req, res, next) {
	
	var purchaseObj = {
	  customer: req.param('customer'),
	  notes: req.param('notes'),
	  purchase_type: req.param('purchase_type')
	}
	
	// Create a Purchase with the params sent from 
	// the sign-up form --> new.ejs
	Purchase.create(purchaseObj, function purchaseCreated(err, purchase) {
	
	  // // If there's an error
	  // if (err) return next(err);
	
	  if (err) {
	    console.log(err);
	    req.session.flash = {
	      err: err
	    }
	
	    // If error redirect back to sign-up page
	    return res.redirect('/purchase/new');
	  }
	
	  // Let other subscribed sockets know that the purchase was created.
	  Purchase.publishCreate(purchase);
	
	    // After successfully creating the purchase
	    // redirect to the show action
	    // From ep1-6: //res.json(purchase); 
		if (req.param('purchase_type') == 'sharpen') {
	    	res.redirect('/customer/updatepunches/' + req.param('customer'));
	    }
	    else {
		    res.redirect('/customer/reloadpunches/' + req.param('customer'));
	    }
	});
	},
	
	initialload: function(req, res, next) {
		var purchaseObj = {
		  customer: req.param('id'),
		  notes: 'Initial punch card purchase',
		  purchase_type: 'initial'
		}
		
		Purchase.create(purchaseObj, function purchaseCreated(err, purchase) {
			if (err) {
		    console.log(err);
		    req.session.flash = {
		      err: err
		    }
		
		    // If error redirect back to sign-up page
		    return res.redirect('/customer');
		  }
		  
		  // Let other subscribed sockets know that the purchase was created.
		  Purchase.publishCreate(purchase);
		  
		  res.redirect('/customer/show/' + req.param('id'));
		});
	},
	
	// render the profile view (e.g. /views/show.ejs)
	show: function(req, res, next) {
	Purchase.findOne(req.param('id'), function foundPurchase(err, purchase) {
	  if (err) return next(err);
	  if (!purchase) return next();
	  res.view({
	    purchase: purchase
	  });
	});
	},
	
	index: function(req, res, next) {
	
	// Get an array of all purchases in the Purchase collection(e.g. table)
	Purchase.find(function foundPurchases(err, purchases) {
	  if (err) return next(err);
	  // pass the array down to the /views/index.ejs page
	  res.view({
	    purchases: purchases
	  });
	});
	},
	
	// render the edit view (e.g. /views/edit.ejs)
	edit: function(req, res, next) {
	
	// Find the purchase from the id passed in via params
	Purchase.findOne(req.param('id'), function foundPurchase(err, purchase) {
	  if (err) return next(err);
	  if (!purchase) return next('Purchase doesn\'t exist.');
	
	  res.view({
	    purchase: purchase
	  });
	});
	},
	
	// process the info from edit view
	update: function(req, res, next) {
	
	var purchaseObj = {
		customer: req.param('customer'),
		notes: req.param('notes'),
	}
	
	Purchase.update(req.param('id'), purchaseObj, function purchaseUpdated(err) {
	  if (err) {
	    return res.redirect('/purchase/edit/' + req.param('id'));
	  }
	
	  res.redirect('/purchase/show/' + req.param('id'));
	});
	},
	
	destroy: function(req, res, next) {
	
	Purchase.findOne(req.param('id'), function foundPurchase(err, purchase) {
	  if (err) return next(err);
	
	  if (!purchase) return next('Purchase doesn\'t exist.');
	
	  Purchase.destroy(req.param('id'), function purchaseDestroyed(err) {
	    if (err) return next(err);
	
	    // Inform other sockets (e.g. connected sockets that are subscribed) that this purchase is now logged in
	    Purchase.publishUpdate(purchase.id, {
	      name: purchase.name,
	      action: ' has been destroyed.'
	    });
	
	    // Let other sockets know that the purchase instance was destroyed.
	    Purchase.publishDestroy(purchase.id);
	
	  });        
	
	  res.redirect('/customer/show/' + req.param('customer'));
	
	});
	}
};

