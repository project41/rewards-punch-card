/**
* Purchase.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
	
  schema: true,

  attributes: {
	customer: {
	    model: 'customer'
	},
	
	notes: {
		type: 'string'
	},
	
	purchase_type: {
		type: 'string',
		required: true
	},
  	
  	toJSON: function() {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }
};

