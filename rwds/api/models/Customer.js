/**
* Customer.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
	
  schema: true,

  attributes: {
	name: {
  		type: 'string',
  		required: true
  	},
  	
  	nicknames: {
	  	type: 'string'
  	},

  	email: {
  		type: 'email',
  		unique: true
  	},
  	
  	punches: {
	  	type: 'integer',
	  	required: true
  	},
  	
  	purchases: {
        collection: 'purchase',
        via: 'customer'
  	},
  	
  	toJSON: function() {
      var obj = this.toObject();
      delete obj._csrf;
      return obj;
    }
  }
};

