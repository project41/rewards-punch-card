$(document).ready(function(){

	// Validate
	// http://bassistance.de/jquery-plugins/jquery-plugin-validation/
	// http://docs.jquery.com/Plugins/Validation/
	// http://docs.jquery.com/Plugins/Validation/validate#toptions

		$('#sign-up-form').validate({
	    rules: {
	      name: {
	        required: true
	      },
	      email: {
	        required: true,
	        email: true
	      },
	      password: {
	      	minlength: 6,
	        required: true
	      },
	      confirmation: {
	      	minlength: 6,
	      	equalTo: "#password"
	      }
	    },
			success: function(element) {
				element
				.text('OK!').addClass('valid')
			}
	  });
	  
	  $('#create-customer-form').validate({
	    rules: {
	      name: {
	        required: true
	      },
	      email: {
	        email: true
	      }
	    },
			success: function(element) {
				element
				.text('OK!').addClass('valid')
			}
	  });
	  
	  $('#create-purchase-form').validate({
	    rules: {
	      purchase_type: {
	        required: true
	      }
	    },
			success: function(element) {
				element
				.text('OK!').addClass('valid')
			}
	  });
	  
	  $('.purchase-date').each(function (idx, elem) {
		  $(elem).text($.format.date($(elem).text(), 'MM/dd/yyyy'));
	  });

});