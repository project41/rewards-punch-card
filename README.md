# Rewards Punch Card App #

This is a super basic app built on [Sails.js](http://sailsjs.org/) and [MongoDB](https://www.mongodb.org/) for managing a punch card rewards system at a local hockey rink. Basic premise is a customer pays for 10 skate sharpenings up front and gets 12. This system allows the rink staff to keep track of how many punches each customer has remaining, reload their cards, and track individual transactions with notes

### Sails Features Utilized ###

* Policies
* Associations
* Authentication
* Session Management

### Dev Environment ###

The dev environment is included within the _vagrant directory which can be used to load up a virtual linux box with nginx, node.js, grunt, bower, and sails preinstalled.